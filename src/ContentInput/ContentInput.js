import React, {useContext} from 'react';
import Context from "../context";
import {Button, ButtonGroup, TextField} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

function useInputValue(defaultValue) {
    const [value, setValue] = React.useState(defaultValue);

    return {
        bind: {
            value,
            onChange: event => setValue(event.target.value),
        },
        clear: () => setValue(defaultValue),
        value: () => value,

    }
}

function ContentInput() {
    const classes = useStyles();

    const {generateQr, resetContent} = useContext(Context)

    const input = useInputValue('')

    function submitForm(event) {
        event.preventDefault();
        generateQr(input.value())
    }

    function resetForm(event) {
        resetContent();
        input.clear();
    }

    return (
        <form className={classes.root} onSubmit={submitForm}>
            <TextField multiline cols="100" rows="5" label="insert content here" variant="outlined" {...input.bind}/>
            <ButtonGroup>
                <Button type="submit">Generate</Button>
                <Button type="reset" onClick={resetForm}>Reset</Button>
            </ButtonGroup>
        </form>
    )
}

export default ContentInput;
