import React from "react";
import {Box} from "@material-ui/core"

function QrDisplay({qrLink}) {
    return (
        <Box>
            {qrLink ? <img src={qrLink} alt="" width="300px"/> : ''}
        </Box>
    );
}

export default QrDisplay;
