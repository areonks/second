import React from 'react';
import './App.css';
import ContentInput from "./ContentInput/ContentInput";
import QrDisplay from "./QrHandler/QrDisplay";
import Context from './context';
import Container from "@material-ui/core/Container";

function App() {
    const [qr, setQr] = React.useState('');

    function generateQr(data) {
        setQr('https://api.qrserver.com/v1/create-qr-code/?size=550x550&data=' + data);
    }

    function resetContent() {
        setQr('');
    }

    return (
        <Context.Provider value={{generateQr, resetContent}}>
            <Container maxWidth={"xs"}>
                <ContentInput/>
                <QrDisplay qrLink={qr}/>
            </Container>
        </Context.Provider>
    );
}

export default App;
